let setSliderActive = (personActive = 0) => {localStorage.setItem('personalActive', personActive)}
let getSliderActive = () => {
    let personActive = localStorage.getItem('personalActive')
    if (personActive) { 
        return personActive
    } else {
        setSliderActive()
        return 0
    }
}

let setNextSlider = () => { 
    let personActive = getSliderActive()
    if (personActive == peopleStorage.length - 1) {
        setSliderActive()
    } else {
        setSliderActive(parseInt(getSliderActive()) + 1)
    }
    generatePeople()
}

let setPrevSlider = () => {
    let personActive = getSliderActive()
    if (personActive == 0) {
        setSliderActive(peopleStorage.length - 1)
    } else {
        setSliderActive(getSliderActive() - 1)
    }
    generatePeople()
}

let generatePerson = (person) => {
    personInfo.description.innerHTML = person.description
    personInfo.name.innerHTML = person.name
    personInfo.work.innerHTML = person.work
    personInfo.img.src = `./img/people/${person.img}`
}


let generatePeople = () => {
    let personActive = getSliderActive()
    peopleList.innerHTML = ""
    generatePerson(peopleStorage[personActive])
    peopleStorage.forEach((person, index) => {
        if (!person.isSkip) {
            peopleList.innerHTML += `
            <img alt="photo" id="img${index}" class="sliderImgItem" src="./img/people/${person.img}">
        `
        }
    })
    let listImg = document.querySelectorAll('.sliderImgItem')
    listImg.forEach((imgElement, index) => {
        if (index == personActive) {
            imgElement.classList.add('sliderActive')
        }
    })
    peopleList.addEventListener('click', (e) => {
        if (e.target.id.length != 0) {
            id = e.target.id
            id = parseInt(id.substring(3))
            setSliderActive(id)
            generatePeople(personActive)
        }
    })
}
