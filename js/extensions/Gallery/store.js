let cartoonsArray = [
    {
        category: "Graphic Design",
        img: "graphic design/graphic-design1.jpg",
        img2: "boxes/Rectangle 24 copy 5.png",
        img3: "boxes/icon.png",
        div: "Graphic Design"
    },
    {
        category: "Graphic Design",
        img: "graphic design/graphic-design2.jpg",
        img2: "boxes/Rectangle 24 copy 5.png",
        img3: "boxes/icon.png",
        div: "Graphic Design"
    },
    {
        category: "Graphic Design",
        img: "graphic design/graphic-design3.jpg",
        img2: "boxes/Rectangle 24 copy 5.png",
        img3: "boxes/icon.png",
        div: "Graphic Design"
    },
    {
        category: "Graphic Design",
        img: "graphic design/graphic-design4.jpg",
        img2: "boxes/Rectangle 24 copy 5.png",
        img3: "boxes/icon.png",
        div: "Graphic Design"
    },
    {
        category: "Graphic Design",
        img: "graphic design/graphic-design5.jpg",
        img2: "boxes/Rectangle 24 copy 5.png",
        img3: "boxes/icon.png",
        div: "Graphic Design"
    },
    {
        category: "Graphic Design",
        img: "graphic design/graphic-design6.jpg",
        img2: "boxes/Rectangle 24 copy 5.png",
        img3: "boxes/icon.png",
        div: "Graphic Design"
    },
    {
        category: "Graphic Design",
        img: "graphic design/graphic-design7.jpg",
        img2: "boxes/Rectangle 24 copy 5.png",
        img3: "boxes/icon.png",
        div: "Graphic Design"
    },
    {
        category: "Graphic Design",
        img: "graphic design/graphic-design8.jpg",
        img2: "boxes/Rectangle 24 copy 5.png",
        img3: "boxes/icon.png",
        div: "Graphic Design"
    },
    {
        category: "Graphic Design",
        img: "graphic design/graphic-design9.jpg",
        img2: "boxes/Rectangle 24 copy 5.png",
        img3: "boxes/icon.png",
        div: "Graphic Design"
    },
    {
        category: "Graphic Design",
        img: "graphic design/graphic-design10.jpg",
        img2: "boxes/Rectangle 24 copy 5.png",
        img3: "boxes/icon.png",
        div: "Graphic Design"
    },
    {
        category: "Graphic Design",
        img: "graphic design/graphic-design11.jpg",
        img2: "boxes/Rectangle 24 copy 5.png",
        img3: "boxes/icon.png",
        div: "Graphic Design"
    },
    {
        category: "Graphic Design",
        img: "graphic design/graphic-design12.jpg",
        img2: "boxes/Rectangle 24 copy 5.png",
        img3: "boxes/icon.png",
        div: "Graphic Design"
    },
    {
        category: "Web Design",
        img: "web design/web-design1.jpg",
        img2: "boxes/Rectangle 24 copy 5.png",
        img3: "boxes/icon.png",
        div: "Web Design"
    },
    {
        category: "Web Design",
        img: "web design/web-design2.jpg",
        img2: "boxes/Rectangle 24 copy 5.png",
        img3: "boxes/icon.png",
        div: "Web Design"
    },
    {
        category: "Web Design",
        img: "web design/web-design3.jpg",
        img2: "boxes/Rectangle 24 copy 5.png",
        img3: "boxes/icon.png",
        div: "Web Design"
    },
    {
        category: "Web Design",
        img: "web design/web-design4.jpg",
        img2: "boxes/Rectangle 24 copy 5.png",
        img3: "boxes/icon.png",
        div: "Web Design"
    },
    {
        category: "Web Design",
        img: "web design/web-design5.jpg",
        img2: "boxes/Rectangle 24 copy 5.png",
        img3: "boxes/icon.png",
        div: "Web Design"
    },
    {
        category: "Web Design",
        img: "web design/web-design6.jpg",
        img2: "boxes/Rectangle 24 copy 5.png",
        img3: "boxes/icon.png",
        div: "Web Design"
    },
    {
        category: "Web Design",
        img: "web design/web-design7.jpg",
        img2: "boxes/Rectangle 24 copy 5.png",
        img3: "boxes/icon.png",
        div: "Web Design"
    },
    {
        category: "Landing Pages",
        img: "landing page/landing-page1.jpg",
        img2: "boxes/Rectangle 24 copy 5.png",
        img3: "boxes/icon.png",
        div: "Landing Pages"
    },
    {
        category: "Landing Pages",
        img: "landing page/landing-page2.jpg",
        img2: "boxes/Rectangle 24 copy 5.png",
        img3: "boxes/icon.png",
        div: "Landing Pages"
    },
    {
        category: "Landing Pages",
        img: "landing page/landing-page3.jpg",
        img2: "boxes/Rectangle 24 copy 5.png",
        img3: "boxes/icon.png",
        div: "Landing Pages"
    },
    {
        category: "Landing Pages",
        img: "landing page/landing-page4.jpg",
        img2: "boxes/Rectangle 24 copy 5.png",
        img3: "boxes/icon.png",
        div: "Landing Pages"
    },
    {
        category: "Landing Pages",
        img: "landing page/landing-page5.jpg",
        img2: "boxes/Rectangle 24 copy 5.png",
        img3: "boxes/icon.png",
        div: "Landing Pages"
    },
    {
        category: "Landing Pages",
        img: "landing page/landing-page6.jpg",
        img2: "boxes/Rectangle 24 copy 5.png",
        img3: "boxes/icon.png",
        div: "Landing Pages"
    },
    {
        category: "Landing Pages",
        img: "landing page/landing-page7.jpg",
        img2: "boxes/Rectangle 24 copy 5.png",
        img3: "boxes/icon.png",
        div: "Landing Pages"
    },
    {
        category: "Wordpress",
        img: "wordpress/wordpress1.jpg",
        img2: "boxes/Rectangle 24 copy 5.png",
        img3: "boxes/icon.png",
        div: "Wordpress"
    },
    {
        category: "Wordpress",
        img: "wordpress/wordpress2.jpg",
        img2: "boxes/Rectangle 24 copy 5.png",
        img3: "boxes/icon.png",
        div: "Wordpress"
    },
    {
        category: "Wordpress",
        img: "wordpress/wordpress3.jpg",
        img2: "boxes/Rectangle 24 copy 5.png",
        img3: "boxes/icon.png",
        div: "Wordpress"
    },
    {
        category: "Wordpress",
        img: "wordpress/wordpress4.jpg",
        img2: "boxes/Rectangle 24 copy 5.png",
        img3: "boxes/icon.png",
        div: "Wordpress"
    },
    {
        category: "Wordpress",
        img: "wordpress/wordpress5.jpg",
        img2: "boxes/Rectangle 24 copy 5.png",
        img3: "boxes/icon.png",
        div: "Wordpress"
    },
    {
        category: "Wordpress",
        img: "wordpress/wordpress6.jpg",
        img2: "boxes/Rectangle 24 copy 5.png",
        img3: "boxes/icon.png",
        div: "Wordpress"
    },
    {
        category: "Wordpress",
        img: "wordpress/wordpress7.jpg",
        img2: "boxes/Rectangle 24 copy 5.png",
        img3: "boxes/icon.png",
        div: "Wordpress"
    },
    {
        category: "Wordpress",
        img: "wordpress/wordpress8.jpg",
        img2: "boxes/Rectangle 24 copy 5.png",
        img3: "boxes/icon.png",
        div: "Wordpress"
    },
    {
        category: "Wordpress",
        img: "wordpress/wordpress9.jpg",
        img2: "boxes/Rectangle 24 copy 5.png",
        img3: "boxes/icon.png",
        div: "Wordpress"
    },
    {
        category: "Wordpress",
        img: "wordpress/wordpress10.jpg",
        img2: "boxes/Rectangle 24 copy 5.png",
        img3: "boxes/icon.png",
        div: "Wordpress"
    },
]