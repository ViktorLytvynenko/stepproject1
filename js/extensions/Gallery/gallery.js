let genereteAllImages = () => {
    galleryList.innerHTML = ``
    cartoonsArray.forEach((imageObject, index) => {
        if (index < countImage) {
            galleryList.innerHTML += `
                <li data-category="graphic-design" class="tabs-title-our-amazing-work-item active relative">
                    <img src="./img/${imageObject.img}" alt="photo" title="photo">
                    <img src="./img/boxes/Rectangle 24 copy 5.png" alt="line" class="absoluteLine">
                    <img src="./img/boxes/icon.png" alt="button" title="button" class="absoluteButton">
                    <div class="absoluteText1 uppercase green-font bold">creative design</div>
                    <div class="absoluteText2">${imageObject.div}</div>
                    </li>
                  `
        }
    })
    loadMoreButton.style.display = "block"
}

let generateCategory = (category) => {
    loadMoreButton.style.display = "none"
    galleryList.innerHTML = ``
    cartoonsArray.forEach((imageObject, index) => {
        if (imageObject.category == category) {
            galleryList.innerHTML += `
                <li data-category="graphic-design" class="tabs-title-our-amazing-work-item active relative">
                    <img src="./img/${imageObject.img}" alt="photo" title="photo">
                    <img src="./img/${imageObject.img2}" alt="line" class="absoluteLine">
                    <img src="./img/${imageObject.img3}" alt="button" title="button" class="absoluteButton">
                    <div class="absoluteText1 uppercase green-font bold">creative design</div>
                    <div class="absoluteText2">${imageObject.div}</div>
                    </li>
                  `
        }
    })
}

let setBtnActive = (activeBtnText) => {
    btnsListGallery.forEach ((btn) => {
        if(btn.innerText != activeBtnText) {
            btn.style.border = "1px solid #DADADA"
            btn.style.color = "#717171"
        } else { 
            btn.style.border = "1px solid #18CFAB"
            btn.style.color = "#18CFAB"
        }
    })
}