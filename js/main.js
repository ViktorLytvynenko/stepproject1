// tabs
let ulTitleElements = document.querySelector(".tabs");
let liTitleElements = ulTitleElements.querySelectorAll(".tabs>li");

let ulContentElements = document.querySelector(".tabs-content");
let liContentElements = ulContentElements.querySelectorAll(".tabs-content>li");

function titleColour() {
    for (let i = 0; i < liTitleElements.length; i++) {
        liTitleElements[i].addEventListener("click", function () {
            for (let j = 0; j < liTitleElements.length; j++) {
                liTitleElements[j].classList.remove("active");
                liContentElements[j].classList.remove("active");
            }
            liTitleElements[i].classList.add("active");
            liContentElements[i].classList.add("active");

        })
    }
}

titleColour(liTitleElements);

// tabs-our-amazing-work
let galleryList = document.getElementById("tabsContent2")
let loadMoreButton = document.getElementById("loadMoreButton")
let spinner = document.getElementById("spinner")
let btnsListGallery = document.querySelectorAll("#categoryGallery")

galleryList.innerHTML = ""
spinner.style.display = "none";
spinner.style.margin = "0 auto";

let countImage = 12;

genereteAllImages()

btnsListGallery.forEach((btn) => {
    btn.addEventListener("click", (e) => {
        if(e.target.innerText.length !=0 && e.target.innerText != "All"){
            generateCategory(e.target.innerText)
        }  else { 
            genereteAllImages()
        }
        setBtnActive(e.target.innerText)
    })
})

loadMoreButton.addEventListener("click", () => {
    if (countImage + 12 <= cartoonsArray.length) {
        spinner.style.display = "block";
        setTimeout(() => {
            spinner.style.display = "none";
            countImage += 12;
            if(countImage >= cartoonsArray.length) {
                loadMoreButton.remove()
            }
            genereteAllImages()
        }, 2000);
    } else {
        loadMoreButton.style.display = "none"
    }
})

// What People Say About theHam

let peopleList = document.querySelector('.peopleList')
let personInfo = {
    description: document.querySelector('#description'),
    name: document.querySelector('#name'),
    work: document.querySelector('#work'),
    img: document.querySelector('#imgPerson')
}
let btns = {
    prev: document.querySelector('#btnPrev'),
    next: document.querySelector('#btnNext')
}

setSliderActive(0)
generatePeople()

btns.prev.addEventListener('click', setPrevSlider)
btns.next.addEventListener('click', setNextSlider)

document.addEventListener('keydown', (event) => {
    if (event.key === "ArrowLeft") {
        setPrevSlider()
    }
    if (event.key === "ArrowRight") {
        setNextSlider()
    }
})

let loadMoreButtonGalleryElement = document.getElementById("loadMoreButtonGallery");
let spinner2 = document.getElementById("spinner2");
let galleryContainer2 = document.querySelector(".gallery-container2");
spinner2.style.display = "none";
galleryContainer2.style.display = "none";
loadMoreButtonGalleryElement.addEventListener("click", function () {
    spinner2.style.display = "block";
    loadMoreButtonGalleryElement.style.display = "none";
    setTimeout(() => {
        spinner2.style.display = "none";
        galleryContainer2.style.display = "flex";

    },2000)
})