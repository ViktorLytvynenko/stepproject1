let peopleStorage = [
    {
        description: 'Integer dignissim, augue tempus ultricies luctus, quam dui laoreet sem, non\n' +
            '                    dictum odio nisi quis massa. Morbi pulvinar odio eget aliquam facilisis. Tempus ultricies\n' +
            '                    luctus, quam dui laoreet sem, non dictum odio nisi quis massa. Morbi pulvinar odio eget aliquam\n' +
            '                    facilisis. The UI/UX Designer will work closely with UI/UX Designers, Business Analysts, developers, and marketing.',
        name: 'Jessy Scott',
        work: 'Ux designer',
        img: 'person1.png'
    },
    {
        description: 'Customer Support is a range of services to assist customers in making cost effective and correct use\n' +
            '                    of a product. It includes assistance in planning, installation, training, troubleshooting,\n' +
            '                    maintenance, upgrading, and disposal of a product. Regarding technology products such as mobile\n' +
            '                    phones, televisions, computers, software products or other electronic or mechanical goods, it is\n' +
            '                    termed technical support.',
        name: 'Husein Bali',
        work: 'Online support',
        img: 'person2.png'
    },
    {
        description: 'A modeling language is any artificial language that can be used to express information, knowledge or\n' +
            '                    systems in a structure that is defined by a consistent set of rules. These rules are used for\n' +
            '                    interpretation of the components within the structure. A software designer or architect may identify\n' +
            '                    a design problem which has been visited and perhaps even solved by others in the past.',
        name: 'Hassan Ali',
        work: 'App design',
        img: 'person3.png'
    },
    {
        description: 'Digital marketing is the component of marketing that uses the Internet and online based digital\n' +
            '                    technologies such as desktop computers, mobile phones and other digital media and platforms to\n' +
            '                    promote products and services. Its development during the 1990s and 2000s changed the way brands and\n' +
            '                    businesses use technology for marketing. As digital platforms became increasingly incorporated into\n' +
            '                    marketing plans and everyday life.',
        name: 'Amanda Meltser',
        work: 'Online marketing',
        img: 'person4.png'
    }
]
